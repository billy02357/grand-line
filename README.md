# grand-line

Web server for hosting magnet links

# Running
```
# clone the repo
git clone git@codeberg.org:billy02357/grand-line.git
cd grand-line
# go into src/ and run main.go
cd src
go run main.go
```

You can also specify the port with
```
go run main.go PORT
```
