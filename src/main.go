package main

import (
	"net/http"
	"io/ioutil"
	"log"
	"os"
	"fmt"
	"github.com/gorilla/mux"
)

func listDir(d string) string {
	var s string
	files, err := ioutil.ReadDir("torrents")
	if err != nil {
		log.Fatal(err)
	}
	for _, f := range files {
		s += "<p><a href=\"/torrent/" + f.Name() + "\">" + f.Name() + "</a></p>\n"
	}
	return s
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "index.html")
}

func main() {
	PORT := ":8081"
	if len(os.Args) > 1 && os.Args[1] != "" {
		PORT = ":" + os.Args[1]
	}
	rtr := mux.NewRouter()
	rtr.HandleFunc("/", homeHandler)

	rtr.HandleFunc("/torrents", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<!DOCTYPE html>\n")
		fmt.Fprintf(w, "<header>\n<title>Grand Line - List of torrents</title>\n</header>\n")
		fmt.Fprintf(w, "<body>\n<h1>List of torrents</h1>\n")
		fmt.Fprintf(w, "<h3>Happy hacking!</h3>\n")
		fmt.Fprintf(w, listDir("torrents"))
		fmt.Fprintf(w, "<a href=\"/\">Go back</a>")
		fmt.Fprintf(w, "</body>")
	})

	rtr.HandleFunc("/torrent/{id:.+}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id := vars["id"]
		fmt.Fprintf(w, "<!DOCTYPE html>\n<html><header><title>Grand Line - %s</title></header></html>", id)
		fmt.Fprintf(w, "<plaintext>")
		http.ServeFile(w, r, "torrents/" + id)
	})

	rtr.HandleFunc("/faq", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "faq.html")
		fmt.Fprintf(w, "<a href=\"/\">Go back</a>")
	})


	http.Handle("/", rtr)

	log.Fatal(http.ListenAndServe(PORT, nil))
}